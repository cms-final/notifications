import pika
import os
import config
from webpush import sendWebPush
import ast

notification_payload = {
    "title": 'Test title!'
}

notification_texts = {
    "orders.add": 'Order {} has been added!',
    "orders.delete": 'Order {} has been deleted!',
    "orders.update": 'Order {} has been changed'
}


def start_consuming():
    print("start_consuming")
    url = config.rabbit_address
    params = pika.URLParameters(url)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    channel.exchange_declare(exchange='actions_log',
                             exchange_type='topic')
    result = channel.queue_declare(exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange='actions_log',
                       queue=queue_name,
                       routing_key='events.add')
    channel.basic_consume(callback_orders,
                          queue=queue_name,
                          no_ack=True)
    channel.start_consuming()


def callback_orders(ch, method, properties, body):
    print(body)
    body_dict = ast.literal_eval(body.decode('utf-8'))
    print("Received notification: {}".format(body_dict))
    sendWebPush(body_dict)
